/*
 *  Created on: 2 maj 2016
 *      Author: Paweł Taborowski
 */
#include <iostream>
#include <getopt.h>
#include <fstream>
#include <ctime>
#include <cstdlib>

//I assume 0 - false, 1 - true, 2 - unknown inside the program and 0 - false, 1 - true, X - unknown in input files
int notOp[3];
int andOp[3][3];
int orOp[3][3];
int implOp[3][3];

//Game progress - not save'able (game isn't long though)
const int LEVELS=4; //Amount of levels implemented
int score[LEVELS];
bool passed[LEVELS];


//Arrays used in all games
char values[] = {'0','1','X'};
//Keep literals in one place.
std::string notStr = "Zaprzeczenie";
std::string andStr = "Koniunkcja";
std::string orStr = "Alternatywa";
std::string implStr = "Implikacja";
std::string operations[] = {notStr, andStr, orStr, implStr};


//Input files
void procNot(); //Process data from file with "not" definition
void proc2arg(int c); //Process data from: c==1 -> AND file, c==2 -> OR file, c==3 -> IMPL file
int conv(char logic); //Converts ['0','1','X'] -> [0,1,2] (values accepted by files into values accepted by application).
void inpFileError(); //Informs about an error and stops application.

void menu(); //Main Menu of the game
void neverending(); //Mini-game 1 – truth of sentences
void lifeIsBrutal(); //Mini-game 2 – modification to previous, possibly more difficult.
void bunchOfNonsense(); //Mini-game 3 - finding true sentences
void howMuchTruth();

int main(int argc, char **argv) {
    //Processing command line options
    static struct option long_options[] =
            {
                    /* These options set a flag. */
                    {"not",  required_argument, 0, 0},
                    {"and",  required_argument, 0, 1},
                    {"or",   required_argument, 0, 2},
                    {"impl", required_argument, 0, 3},
                    {0, 0,                      0, 0}
            };

    for (int i = 0; i < 4; i++) {
        int option_index;
        int c = getopt_long(argc, argv, "", long_options, &option_index);
        switch(c){
            case -1:
                std::cerr << "Wywołanie: " << argv[0] << " --not nazwa1.txt --and nazwa2.txt --or nazwa3.txt --impl nazwa4.txt\n";
                return 1;
            case 0:
                procNot();
                break;
            case 1:
            case 2:
            case 3:
                proc2arg(c);
                break;
        }
    }

    //Introduction
    std::cout << "\n\n„Bądź ostrożny – w swoich sądach.”\n – Józef Maria Bocheński „Podręcznik mądrości tego świata”\n\n[Naciśnij ENTER, aby rozpocząć]";

    std::cin.ignore(100,'\n');

    std::cout << "\n\n– Więc zdecydowałeś się dołączyć do Grupy Logików Polskich?\nNie jest to łatwe, musisz przejść nasze próby.\nCzy wykażesz się umiejętnością logicznego myślenia i determinacją? Przekonajmy się.\nKtórej próby chcesz się podjąć w pierwszej kolejności?";

    //Initializing game progress
    for (int i=0; i<LEVELS; i++){
        score[i] = 0;
        passed[i] = false;
    }

    menu(); //Starts the actual game.

    return 0;
}

void procNot(){
    //pre-filling table with invalid values
    for (int i = 0; i < 3; i++) notOp[i] = -6; //Sum remains negative if any of inputs for NOT is not provided.
    std::fstream notFile;
    notFile.open(optarg, std::fstream::in);
    for (int i=0; i<3; i++){
        char key, value;
        notFile >> key >> value;
        int keyInt = conv(key);
        int valInt = conv(value);
        if (keyInt == -1 or valInt == -1){
            notFile.close();
            inpFileError();
        }
        notOp[keyInt]=valInt;
    }

    //checking correctness...
    int sum=0;
    for (int i=0;i<3;i++) sum += notOp[i];
    if (sum < 0){
        notFile.close();
        inpFileError();
    }
    notFile.close();
}

void proc2arg(int c){
    //Make it suitable for 3 possible inputs
    int* oper;
    if (c==1){
        oper = &andOp[0][0];
    }else if(c==2){
        oper = &orOp[0][0];
    }else{ //Function is only called by a program, and parameter can only equal to 1, 2 or 3.
        oper = &implOp[0][0];
    }

    for (int i = 0; i < 9; i++) oper[i] = -18; //Sum remains negative if any of inputs for NOT is not provided.

    std::fstream inpFile;
    inpFile.open(optarg, std::fstream::in);
    for (int i=0; i<9; i++){
        char key1, key2, value;
        inpFile >> key1 >> key2 >> value;
        int key1Int = conv(key1);
        int key2Int = conv(key2);
        int valInt = conv(value);
        if (key1Int == -1 or key2Int == -1 or valInt == -1){
            inpFile.close();
            inpFileError();
        }
        oper[key1Int * 3 + key2Int] = valInt;
    }

    //checking correctness...
    int sum=0;
    for (int i=0;i<9;i++) sum += oper[i];
    if (sum < 0){
        inpFile.close();
        inpFileError();
    }
    inpFile.close();
}

int conv(char logic){
    switch(logic){
        case '0':
            return 0;
        case '1':
            return 1;
        case 'X':
        case 'x':
            return 2;
        default:
            return -1; //error.
    }
}

void inpFileError(){
    std::cerr << "Nieprawidłowy format pliku " << optarg << ".\n";
    exit(2);
}

void menu(){
    while(true) {
        //Is the game finished? - after minigame we're retuning inside loop, so check must be performed inside loop as well.
        int finished=0;
        for (int i=0; i<LEVELS; i++) if (passed[i]) finished++;
        if (finished == LEVELS) std::cout << "\n\n– Gratulacje!\nUkończyłeś wszystkie próby i tym samym uzyskałeś członkostwo w naszej Grupie!\nOczywiście jeśli chcesz, możesz starać się poprawić wynik poszczególnych testów.";

        std::cout << "\n\n1. Niekończąca się opowieść [WYNIK: " << score[0] << "]";
        if (passed[0]) std::cout << "[UKOŃCZONA]";

        std::cout << "\n2. Życie jest brutalne [WYNIK: " << score[1] << "]";
        if (passed[1]) std::cout << "[UKOŃCZONA]";

        std::cout << "\n3. Stek bzdur [WYNIK: " << score[2] << "]";
        if (passed[2]) std::cout << "[UKOŃCZONA]";

        std::cout << "\n4. Ileż w tym prawdy? [WYNIK: " << score[3] << "]";
        if (passed[3]) std::cout << "[UKOŃCZONA]";

        std::cout << "\n0. Wyjście.";

        char answer='a';
        std::cout << "\n\nOdpowiedź (wprowadź liczbę, zatwierdź ENTERem): ";
        std::cin >> answer;
        std::cin.ignore(100,'\n');
        switch (answer) {
            case '0':
                exit(0);
            case '1':
                neverending();
                break;
            case '2':
                lifeIsBrutal();
                break;
            case '3':
                bunchOfNonsense();
                break;
            case '4':
                howMuchTruth();
                break;
        }
    }
}

void neverending(){
    std::cout << "\n– Podczas tej próby, wszystko co powiesz może zostać użyte przeciwko Tobie – pilnuj więc logiki swoich wypowiedzi!\n";
    std::cout << "Na start otrzymasz dwie wartości logiczne i operację którą należy wykonać na ostatnich wartościach w ilości potrzebnej do danej operacji – niczym w odwrotnej notacji polskiej, jednak działania zawsze wykonujemy na symbolach od góry.\n"
    << "Jeśli się pomylisz - test się zakończy; jeśli odpowiesz poprawnie – Twoja odpowiedź będzie nowym argumentem kolejnej zażądanej operacji.\n"
    << "Od momentu udzielenia pierwszej odpowiedzi masz na zadanie 30 sekund. By zdać, musisz uzyskać co najmniej 20 punktów\n"
    << "Oczywiście przy niektórych definicjach operatorów logicznych, zadanie jest łatwiejsze niż przy innych...";

    bool justStarted=true;

    srand((unsigned int) time(NULL));
    char arg1 = values[rand()%3];
    char arg2 = values[rand()%3];
    std::string oper = operations[rand()%4];
    int points=-1;
    std::cout << "\n" << arg1 << "\n" << arg2 << " " << oper << "\n";

    time_t timer;

    while(true){
        points++;
        char ans;
        std::cin >> ans;
        if(justStarted){ timer = time(NULL); justStarted=false; }
        if (time(NULL) - timer > 30) {
            std::cout << "\nPo czasie...";
            break;
        }else if((oper == notStr and notOp[conv(arg2)] == conv(ans)) or
           (oper == andStr and andOp[conv(arg1)][conv(arg2)] == conv(ans)) or
           (oper == orStr and orOp[conv(arg1)][conv(arg2)] == conv(ans)) or
           (oper == implStr and implOp[conv(arg1)][conv(arg2)] == conv(ans))){
            arg1 = arg2;
            arg2 = ans;
            oper = operations[rand()%4];
            std::cout << " " << oper << "\n";
        }else{
            std::cout << "\nNiestety nie...";
            break;
        }
    }

    //Score
    std::cout << "\n– Twój wynik to " << points << " punktów.";

    //New record?
    if (points > score[0]){
        score[0]=points;
        std::cout << "\n– Najlepszy wynik w historii!";
    }

    //Test passed?
    if (!passed[0] and points >= 20){
        passed[0] = true;
        std::cout << "\n– Gratulacje! Test zaliczony!";
    }
}

void lifeIsBrutal(){
    std::cout << "\n– Ta próba to modyfikacja poprzedniej – mam nadzieję, że tamtą masz już za sobą?\n"
              << "Tym razem po każdej Twojej odpowiedzi JA podam jeszcze jeden symbol i dopiero zadam pytanie.\n"
              << "Nie każda logika, trywializująca poprzednie zadanie, uprości też to..."
              << "Ilość czasu to ponownie 30 sekund... No dobra niech Ci będzie, zaiczam od 15 punktów.";

    bool justStarted=true; //We didn't start the timer yet.

    srand((unsigned int) time(NULL));
    char arg1 = values[rand()%3];
    char arg2 = values[rand()%3];
    std::string oper = operations[rand()%4];
    int points=-1;
    std::cout << "\n" << arg1 << "\n" << arg2 << " " << oper << "\n";

    time_t timer;

    while(true){
        points++;
        char ans;
        std::cin >> ans;
        if(justStarted){ timer = time(NULL); justStarted=false; }
        if (time(NULL) - timer > 30) {
            std::cout << "\nPo czasie...";
            break;
        }else if((oper == notStr and notOp[conv(arg2)] == conv(ans)) or
                 (oper == andStr and andOp[conv(arg1)][conv(arg2)] == conv(ans)) or
                 (oper == orStr and orOp[conv(arg1)][conv(arg2)] == conv(ans)) or
                 (oper == implStr and implOp[conv(arg1)][conv(arg2)] == conv(ans))){
            arg1 = ans;
            arg2 = values[rand()%3];
            oper = operations[rand()%4];
            std::cout << arg2 << " " << oper << "\n";
        }else{
            std::cout << "\nNiestety nie...";
            break;
        }
    }

    //Score.
    std::cout << "\n– Twój wynik to " << points << " punktów.";

    //New record?
    if (points > score[1]){
        score[1]=points;
        std::cout << "\n– Najlepszy wynik w historii!";
    }

    //Test passed?
    if (!passed[1] and points >= 15){
        passed[1] = true;
        std::cout << "\n– Gratulacje! Test zaliczony!";
    }
}

void bunchOfNonsense(){
    std::cout << "\n– Same bzdury piszą ostatnio w tych gazetach! Spójrz no tutaj.\n"
              << "Wskaż mi, na której pozycji zaczyna się pierwsze zdanie danego typu, któremu prawidłowo przypisano, czy jest prawdziwe.\n"
              << "[Na przykład dla implikacji ciąg \"0 0 ";
    if(implOp[0][0]!=2) std::cout << implOp[0][0];
    else std::cout << "X";
    std::cout << "\" spełnia warunki zadania, gdyż, przy podanych definicjach, zdanie \"z fałszu wynika fałsz\" ";
    switch(implOp[0][0]){
        case 0:
            std::cout << "jest nieprawdziwe";
            break;
        case 1:
            std::cout << "jest prawdziwe";
            break;
        case 2:
            std::cout << "nie ma określonej prawdziwości";
            break;
    }
    std::cout << ".]\n"
              << "Jeśli uważasz, że nie ma żadnego, napisz 0.\n"
              << "W ciągu 30 sekund musisz przewertować przynajmniej 5 gazet.\n";

    const int LENGTH=10; //Amount of symbols in line.
    bool justStarted=true; //We didn't start the timer yet.
    time_t timer;
    int points = 0;

    while(true) {
        //Display task
        srand((unsigned int) time(NULL));
        std::string oper = operations[rand() % 4];
        char words[LENGTH];
        std::cout << "\n" << oper << ":\n";
        for (int i = 0; i < LENGTH; i++) {
            words[i] = values[rand() % 3];
            std::cout << " " << words[i] << " ";
        }
        std::cout << "\n";
        for (int i = 1; i < LENGTH + 1; i++) {
            std::cout << " " << i << " ";
        }
        std::cout << "\nPozycja: ";

        //Check
        int ans;
        std::cin >> ans;
        while(std::cin.fail()){
            std::cin.clear(); //Reset the flags.
            std::cin.ignore(100, '\n');
            std::cout << "\nOpowiadając takie bzdury marnujesz tylko czas. Podaj pozycję!\n";
            std::cin >> ans; //Retry
        }

        int corAns = 0;//correct answer (after IFs)
        if (oper == notStr) {
            for (int i = 0; i < LENGTH - 1; i++) {
                if (notOp[conv(words[i])] == conv(words[i + 1])) {
                    corAns = i + 1;
                    break;
                }
            }
        } else if (oper == andStr) {
            for (int i = 0; i < LENGTH - 2; i++) {
                if (andOp[conv(words[i])][conv(words[i + 1])] == conv(words[i + 2])) {
                    corAns = i + 1;
                    break;
                }
            }
        } else if (oper == orStr) {
            for (int i = 0; i < LENGTH - 2; i++) {
                if (orOp[conv(words[i])][conv(words[i + 1])] == conv(words[i + 2])) {
                    corAns = i + 1;
                    break;
                }
            }
        } else {//impl
            for (int i = 0; i < LENGTH - 2; i++) {
                if (implOp[conv(words[i])][conv(words[i + 1])] == conv(words[i + 2])) {
                    corAns = i + 1;
                    break;
                }
            }
        }

        if (ans != corAns){
            std::cout << "\nNiestety nie... Poprawna odpowiedź to " << corAns << ".";
            break;
        }

        if(justStarted){ timer = time(NULL); justStarted=false; }
        if (time(NULL) - timer > 30) {
            std::cout << "\nPo czasie...";
            break;
        }

        points++;
    }

    //On defeat

    std::cout << "\n– Twój wynik to " << points << " punktów.";

    //New record?
    if (points > score[2]){
        score[2]=points;
        std::cout << "\n– Najlepszy wynik w historii!";
    }

    //Test passed?
    if (!passed[2] and points >= 5){
        passed[2] = true;
        std::cout << "\n– Gratulacje! Test zaliczony!";
    }
}

void howMuchTruth(){
    std::cout << "\n– Pytałem Cię już, czy widzisz jakąś prawdę w tych gazetach (poprzedni test)? Tym razem chciałbym się od Ciebie dowiedzieć, ile jest tu tej prawdy...\n"
              << "Ile prawdziwych zdań danego typu jest na tej stronie? Zdania mogą się zazębiać.\n"
              << "Jeśli uważasz, że masz dość talentu, by do nas dołączyć, 30 sekund wystarczy Ci na 3 gazety... Chyba teraz się nie poddasz...? Stoper startuje po pierwszej odpowiedzi.\n";

    const int LENGTH=10; //Amount of symbols in line.
    bool justStarted=true; //We didn't start the timer yet.
    time_t timer;
    int points = 0;

    while(true) {
        //Display task
        srand((unsigned int) time(NULL));
        std::string oper = operations[rand() % 4];
        char words[LENGTH];
        std::cout << "\n" << oper << ":\n";
        for (int i = 0; i < LENGTH; i++) {
            words[i] = values[rand() % 3];
            std::cout << " " << words[i] << " ";
        }
        std::cout << "\nIlość: ";

        //Check
        int ans;
        std::cin >> ans;
        while(std::cin.fail()){
            std::cin.clear(); //Reset the flags.
            std::cin.ignore(100, '\n');
            std::cout << "\nOpowiadając takie bzdury marnujesz tylko czas. Podaj pozycję!\n";
            std::cin >> ans; //Retry
        }

        int corAns = 0;//correct answer (after IFs)
        if (oper == notStr) {
            for (int i = 0; i < LENGTH - 1; i++) {
                if (notOp[conv(words[i])] == conv(words[i + 1])) {
                    corAns++;
                }
            }
        } else if (oper == andStr) {
            for (int i = 0; i < LENGTH - 2; i++) {
                if (andOp[conv(words[i])][conv(words[i + 1])] == conv(words[i + 2])) {
                    corAns++;
                }
            }
        } else if (oper == orStr) {
            for (int i = 0; i < LENGTH - 2; i++) {
                if (orOp[conv(words[i])][conv(words[i + 1])] == conv(words[i + 2])) {
                    corAns++;
                }
            }
        } else {//impl
            for (int i = 0; i < LENGTH - 2; i++) {
                if (implOp[conv(words[i])][conv(words[i + 1])] == conv(words[i + 2])) {
                    corAns++;
                }
            }
        }

        if (ans != corAns){
            std::cout << "\nNiestety nie... Poprawna odpowiedź to " << corAns << ".";
            break;
        }

        if(justStarted){ timer = time(NULL); justStarted=false; }
        if (time(NULL) - timer > 30) {
            std::cout << "\nPo czasie...";
            break;
        }

        points++;
    }

    //On defeat
    std::cout << "\n– Twój wynik to " << points << " punktów.";
    //New record?
    if (points > score[3]){
        score[3]=points;
        std::cout << "\n– Najlepszy wynik w historii!";
    }

    //Test passed?
    if (!passed[3] and points >= 3){
        passed[3] = true;
        std::cout << "\n– Gratulacje! Test zaliczony!";
    }
}
