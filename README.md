# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* 2 projects for the Logika Matematyczna w Informatyce course.
* Version 1.0

### How do I get set up? ###
* For "if" project:
    + Compilation (assuming you are in "if" sub-folder)
        - For res.c: gcc res.c -ansi -pedantic -std=c11 -Wall -W -Wconversion -Wshadow -Wcast-qual -Wwrite-strings -o res
        - For resNeg.c gcc resNeg.c -ansi -pedantic -std=c11 -Wall -W -Wconversion -Wshadow -Wcast-qual -Wwrite-strings -o resNeg
          (You can name resNeg executable "res" as well, so you could easily launch test)
    + Dependencies
        - gcc supporting standard C-11
        - You are expected to use Linux for launching tests in bash
    + How to run tests
        - In bash, in if sub-folder: ./test.sh 
          (if all tests success, there will be no output)

* For "game" project:
    + Compilation (assuming you are in "game" sub-folder)
        - g++ game.cpp -o game
    + Dependencies
        - g++
        - GNU C Library (on Linux installed by default)

### Contribution guidelines ###

* No contributions are required/expected/possible.

### Who do I talk to? ###

* Repo owner