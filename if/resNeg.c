/*
  Author: Paweł Taborowski

  The program helps to choose whether you should use a photo (or image, or movie) in high resolution or low resolution (for example in your external application). It prints "high","low" or "error" on standard output and return exitcode, accordingly 0, 1 or 2.

  Command: executable_name X_screen_resolution Y_screen_resolution X_photo_resolution Y_photo_resolution photo_importance detailed_photo

  executable_name – name of executable containing this program after compilation.
  X_screen_resolution – horizontal resolution of a target device.
  Y_screen_resolution – vertical resolution of a target device.
  X_photo_resolution – native horizontal resolution of a photo.
  Y_photo_resolution – native vertical resolution of a photo.
  photo_importance – Is this photo important [y/n]? Case unsensitive, answer 'n' forces low resolution
  detailed_photo – Does the photo contain very small details [y/n]? Case unsensitive, answer 'y' forces high resolution unless photo is not important
*/

//gcc res.c -ansi -pedantic -std=c11 -Wall -W -Wconversion -Wshadow -Wcast-qual -Wwrite-strings -o res

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int X_TRES = 640;
const int Y_TRES = 480;

void error(void); //In case of invalid input

int main(int argc, char* argv[]){
  //Parse the input
  if (argc < 7) error();

  int devX, devY; // device's X && Y resolution
  if ((devX=atoi(argv[1])) == 0) error();
  if ((devY=atoi(argv[2])) == 0) error();

  int phX, phY; // photo's X and Y resolution
  if ((phX=atoi(argv[3])) == 0) error();
  if ((phY=atoi(argv[4])) == 0) error();

  int important; // photo's importance
  if (strcmp(argv[5],"y")==0 || strcmp(argv[5],"Y")==0) important=1;
  else if (strcmp(argv[5],"n")==0 || strcmp(argv[5],"N")==0) important=0;
  else error();

  int detailed; // is photo detailed?
  if (strcmp(argv[6],"y")==0 || strcmp(argv[6],"Y")==0) detailed=1;
  else if (strcmp(argv[6],"n")==0 || strcmp(argv[6],"N")==0) detailed=0;
  else error();

  //Main if-statement
  if (important != 1 || (detailed != 1 && (devX <= X_TRES || devY <= Y_TRES || phX <= X_TRES || phY <= Y_TRES))){
    printf("low\n");
    return 1;
  }else{
    printf("high\n");
    return 0;
  }
}

void error(void){
  printf("error\n");
  exit(2);
}
